ARG CORE_SDK=mcr.microsoft.com/dotnet/core/sdk
ARG ASPNET=mcr.microsoft.com/dotnet/core/aspnet
ARG VERSION=3.1

FROM $CORE_SDK:$VERSION AS build-env
WORKDIR /app
COPY . ./
RUN dotnet restore -s ../../NugetPackages -s https://api.nuget.org/v3/index.json
RUN dotnet publish -c Release -o ./out

FROM $ASPNET:$VERSION AS runtime
WORKDIR /app
COPY src/Movies.Championship.Api/appsettings*.json ./
COPY --from=build-env /app/out ./
ADD Pottencial-Certificate-Authority.crt /usr/local/share/ca-certificates/Pottencial-Certificate-Authority.crt
RUN chmod 644 /usr/local/share/ca-certificates/Pottencial-Certificate-Authority.crt && update-ca-certificates
RUN sed -i -e "s|MinProtocol = TLSv1.2|MinProtocol = TLSv1.0 |g" /etc/ssl/openssl.cnf
RUN sed -i -e "s|CipherString = DEFAULT@SECLEVEL=2|CipherString = DEFAULT@SECLEVEL=1 |g" /etc/ssl/openssl.cnf
EXPOSE 80

ENTRYPOINT dotnet Movies.Championship.Api.dll
