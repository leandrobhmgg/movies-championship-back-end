﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Movies.ChampionShip.Application.Mappers;

namespace Movies.Championship.Api.Configurations
{
    public static class AutoMapperConfig
    {
        public static void ConfigureAutoMapper(this IServiceCollection services)
        {
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });
            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}
