﻿using Microsoft.Extensions.DependencyInjection;
using Movies.ChampionShip.Application.Core.Entities;
using Movies.ChampionShip.Application.Core.Interfaces;
using Movies.ChampionShip.Application.Service;
using Movies.ChampionShip.Application.Service.Interfaces;

namespace Movies.Championship.Api.Configurations
{

    public static class ServiceConfig
    {
        public static void ConfigureService(this IServiceCollection services)
        {
            services.AddTransient<IMovieService, MovieService>();
            services.AddTransient<IMovieChampionship, MovieChampionship>();
        }
    }
}
