﻿using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;
using Movies.ChampionShip.Application.Dtos.Request;
using Movies.ChampionShip.Application.Validator;

namespace Movies.Championship.Api.Configurations
{
    public static class ValidatorConfig
    {
        public static void ConfigureValidator(this IServiceCollection services)
        {

            services.AddMvc()
               .AddFluentValidation(fvc =>
                           fvc.RegisterValidatorsFromAssemblyContaining<Startup>());

            services.AddScoped<IValidator<ChampionshipRequest>, ChampionshipRequestValidator>();
        }
    }
}
