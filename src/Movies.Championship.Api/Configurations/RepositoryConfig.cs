﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Movies.ChampionShip.Application.Repository;
using Movies.ChampionShip.Application.Repository.Interfaces;
using System;

namespace Movies.Championship.Api.Configurations
{
    public static class RepositoryConfig
    {
        private const string AppSettingsUrl = "Configuration:URL_API";
        public static void ConfigureRepository(this IServiceCollection services, IConfiguration configuration)
        {


            services.AddHttpClient<IMovieRepository, MovieRepository>(client =>
            {
                client.BaseAddress = new Uri(configuration.GetValue<string>(AppSettingsUrl));
            });

        }
    }
}
