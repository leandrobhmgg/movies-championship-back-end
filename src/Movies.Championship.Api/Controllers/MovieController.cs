﻿using Microsoft.AspNetCore.Mvc;
using Movies.ChampionShip.Application.Dtos.Request;
using Movies.ChampionShip.Application.Dtos.Response;
using Movies.ChampionShip.Application.Service.Interfaces;
using System.Threading.Tasks;

namespace Movies.Championship.Api.Controllers
{
    [Route("api/movie")]
    [ApiController]
    public class MovieController : Controller
    {
        private readonly IMovieService _movieService;
        public MovieController
        (
            IMovieService movieService
        )
        {
            _movieService = movieService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(MovieResponse[]), 200)]
        public async Task<IActionResult> Get()
        {
            return Ok(await _movieService.GetAsync());
        }

        [HttpPost("play")]
        [ProducesResponseType(typeof(ChampionshipResponse), 200)]
        public async Task<IActionResult> Play(ChampionshipRequest request)
        {
            return Ok(await _movieService.PlayAsync(request));
        }
    }
}
