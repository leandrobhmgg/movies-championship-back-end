﻿using Microsoft.AspNetCore.Mvc;

namespace Movies.Championship.Api.Controllers
{
    [Route("api/healthcheck")]
    [ApiController]
    public class HealthCheckController : Controller
    {

        [HttpGet("ping")]
        [ProducesResponseType(200)]
        public IActionResult GetPing()
        {
            return Ok();
        }
    }
}
