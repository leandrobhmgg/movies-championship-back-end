﻿namespace Movies.ChampionShip.Application.Dtos.Request
{
    public class ChampionshipRequest
    {
        public string[] ids { get; set; }
    }
}
