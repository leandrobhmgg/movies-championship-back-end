﻿namespace Movies.ChampionShip.Application.Dtos.Response
{
    public class ChampionshipResponse
    {

        public ChampionResponse Champion { get; set; }
        public ViceChampionResponse ViceChampion { get; set; }
    }

    public class ChampionResponse : MovieResponse
    {


    }

    public class ViceChampionResponse : MovieResponse
    {

    }
}
