﻿using System;

namespace Movies.ChampionShip.Application.Dtos.Response
{
    public class MovieResponse
    {

        public string Id { get; set; }
        public string Title { get; set; }
        public Int16 Year { get; set; }
        public decimal Score { get; set; }
        public bool Selected { get; set; }
    }
}
