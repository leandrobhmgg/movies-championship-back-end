﻿using Movies.ChampionShip.Application.Core.Entities;
using System.Collections.Generic;

namespace Movies.ChampionShip.Application.Core.Interfaces
{
    public interface IMovieChampionship
    {
        MovieMatch Play(IEnumerable<Movie> movies);
    }
}
