﻿using Movies.ChampionShip.Application.Core.Interfaces;
using Movies.ChampionShip.Application.Properties;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Movies.ChampionShip.Application.Core.Entities
{
    public class MovieChampionship : IMovieChampionship
    {
        private MovieMatch Winner { get; set; }
        public MovieMatch Play(IEnumerable<Movie> movies)
        {
            var groups = GenerateInitialGroups(movies);
            StartChampionship(groups);
            return Winner;
        }

        private List<MovieMatch> GenerateInitialGroups(IEnumerable<Movie> movies)
        {
            var moviesOrder = movies.OrderBy(x => x.Title).ToList();

            ValidateNumberOfCompetitors(moviesOrder.Count());

            List<MovieMatch> groups = new List<MovieMatch>();

            do
            {
                var first = moviesOrder.First();
                var last = moviesOrder.Last();

                var group = new MovieMatch(first, last);

                groups.Add(group);

                moviesOrder.Remove(first);
                moviesOrder.Remove(last);


            } while (moviesOrder.Count() > 0);
            return groups;
        }

        private void ValidateNumberOfCompetitors(int moviesCount)
        {
            if (moviesCount % 2 != 0 ||
                (moviesCount / 2) % 2 != 0
                )
                throw new Exception(Resources.NumberMoviesMustBeEven);

        }

        private void StartChampionship(List<MovieMatch> movieGroups)
        {
            List<MovieMatch> groups = new List<MovieMatch>();
            do
            {
                var first = movieGroups.First().Winner;
                movieGroups.Remove(movieGroups.First());
                var last = movieGroups.First().Winner;
                movieGroups.Remove(movieGroups.First());

                var group = new MovieMatch(first, last);

                groups.Add(group);

            } while (movieGroups.Count() > 0);

            if (groups.Count > 1)
                StartChampionship(groups);

            else
                Winner = groups.First();

        }
    }
}
