﻿using System.Collections.Generic;

namespace Movies.ChampionShip.Application.Core.Entities
{
    public class MovieMatch
    {
        public List<Movie> movies { get; set; }
        public Movie Winner { get; set; }

        public MovieMatch(Movie fisrt, Movie last)
        {
            movies = new List<Movie>();
            movies.Add(fisrt);
            movies.Add(last);
            Winner = fisrt.Score > last.Score ? fisrt : last;
        }
    }
}
