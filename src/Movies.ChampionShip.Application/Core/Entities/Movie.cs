﻿using System;

namespace Movies.ChampionShip.Application.Core.Entities
{
    public class Movie
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public Int16 Year { get; set; }
        public decimal Score { get; set; }
    }
}
