﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Movies.ChampionShip.Application.Repository.DataModel;
using Movies.ChampionShip.Application.Repository.Interfaces;
using Newtonsoft.Json;

namespace Movies.ChampionShip.Application.Repository
{
    public class MovieRepository : IMovieRepository
    {
        private readonly HttpClient _httpClient;
        public MovieRepository
        (
                HttpClient httpClient
        )
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<MovieDataModel>> GetAsync()
        {

            using var response = await _httpClient.GetAsync("filmes");

            if (response.IsSuccessStatusCode)
            {
                var customerJsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<MovieDataModel>>(customerJsonString);
            }
            else
            {
                return null;
            }

        }
    }
}
