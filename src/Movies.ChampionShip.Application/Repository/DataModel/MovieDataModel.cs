﻿using Newtonsoft.Json;

namespace Movies.ChampionShip.Application.Repository.DataModel
{
    public class MovieDataModel
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "titulo")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "ano")]
        public int Year { get; set; }

        [JsonProperty(PropertyName = "nota")]
        public decimal Score { get; set; }
    }
}
