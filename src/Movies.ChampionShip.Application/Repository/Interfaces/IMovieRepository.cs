﻿using Movies.ChampionShip.Application.Repository.DataModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Movies.ChampionShip.Application.Repository.Interfaces
{
    public interface IMovieRepository
    {
        Task<IEnumerable<MovieDataModel>> GetAsync();
    }
}
