﻿using AutoMapper;
using Movies.ChampionShip.Application.Core.Entities;
using Movies.ChampionShip.Application.Dtos.Response;
using System.Linq;

namespace Movies.ChampionShip.Application.Mappers.Converters
{

    public class ChampionshipResponseTypeConverter : ITypeConverter<MovieMatch, ChampionshipResponse>
    {
        public ChampionshipResponse Convert(MovieMatch source, ChampionshipResponse destination, ResolutionContext context)
        {
            destination = new ChampionshipResponse();

            var champion = source.movies.Where(x => x.Id == source.Winner.Id).FirstOrDefault();
            var vice = source.movies.Where(x => x.Id != source.Winner.Id).FirstOrDefault();

            destination.Champion = new ChampionResponse
            {
                Id = champion.Id,
                Title = champion.Title,
                Year = champion.Year,
                Score = champion.Score
            };

            destination.ViceChampion = new ViceChampionResponse
            {
                Id = vice.Id,
                Title = vice.Title,
                Year = vice.Year,
                Score = vice.Score
            };


            return destination;
        }
    }
}
