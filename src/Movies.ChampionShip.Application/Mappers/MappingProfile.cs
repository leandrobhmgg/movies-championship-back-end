﻿using AutoMapper;
using Movies.ChampionShip.Application.Core.Entities;
using Movies.ChampionShip.Application.Dtos.Response;
using Movies.ChampionShip.Application.Mappers.Converters;
using Movies.ChampionShip.Application.Repository.DataModel;

namespace Movies.ChampionShip.Application.Mappers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<MovieDataModel, MovieResponse>().ReverseMap();
            CreateMap<MovieDataModel, Movie>().ReverseMap();
            CreateMap<MovieMatch, ChampionshipResponse>().ConvertUsing(new ChampionshipResponseTypeConverter());
        }
    }
}
