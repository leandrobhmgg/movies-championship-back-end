﻿using FluentValidation;
using Movies.ChampionShip.Application.Dtos.Request;
using Movies.ChampionShip.Application.Properties;

namespace Movies.ChampionShip.Application.Validator
{
    public class ChampionshipRequestValidator : AbstractValidator<ChampionshipRequest>
    {
        public ChampionshipRequestValidator()
        {
            RuleFor(x => x.ids)
                .NotEmpty()
                .WithMessage(Resources.ValidateRequiredChampionshipRequestIds)
                .Must(x => x?.Length == 8)
                .WithMessage(Resources.ValidateLengthChampionshipRequestIds);
        }
    }
}
