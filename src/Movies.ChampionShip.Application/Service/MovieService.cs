﻿using AutoMapper;
using Movies.ChampionShip.Application.Core.Entities;
using Movies.ChampionShip.Application.Core.Interfaces;
using Movies.ChampionShip.Application.Dtos.Request;
using Movies.ChampionShip.Application.Dtos.Response;
using Movies.ChampionShip.Application.Repository.Interfaces;
using Movies.ChampionShip.Application.Service.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Movies.ChampionShip.Application.Service
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IMapper _mapper;
        private readonly IMovieChampionship _movieChampionship;
        public MovieService
        (
                IMovieRepository movieRepository,
                IMapper mapper,
                IMovieChampionship movieChampionship
        )
        {
            _movieRepository = movieRepository;
            _mapper = mapper;
            _movieChampionship = movieChampionship;
        }


        public async Task<IEnumerable<MovieResponse>> GetAsync()
        {
            return _mapper.Map<IEnumerable<MovieResponse>>(await _movieRepository.GetAsync());
        }

        public async Task<ChampionshipResponse> PlayAsync(ChampionshipRequest request)
        {

            var movies = _mapper.Map<IEnumerable<Movie>>(await _movieRepository.GetAsync()).Where(x => request.ids.Contains(x.Id));

            var resultPlay = _movieChampionship.Play(movies);

            return _mapper.Map<ChampionshipResponse>(resultPlay);
        }

    }
}
