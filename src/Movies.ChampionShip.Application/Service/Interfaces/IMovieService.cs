﻿using Movies.ChampionShip.Application.Dtos.Request;
using Movies.ChampionShip.Application.Dtos.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Movies.ChampionShip.Application.Service.Interfaces
{
    public interface IMovieService
    {
        Task<IEnumerable<MovieResponse>> GetAsync();

        Task<ChampionshipResponse> PlayAsync(ChampionshipRequest request);
    }
}
