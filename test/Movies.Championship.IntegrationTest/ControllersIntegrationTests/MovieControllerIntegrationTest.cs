﻿using System.Linq;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using Refit;
using Movies.Championship.IntegrationTest.Base;
using Movies.Championship.Api;
using Movies.Championship.IntegrationTest.Base.Refit;
using Movies.ChampionShip.Application.Dtos.Request;

namespace Movies.Championship.IntegrationTest.ControllersIntegrationTests
{
    public class MovieControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly IRefitMovie _refitMovie;
        public MovieControllerIntegrationTest(CustomWebApplicationFactory<Startup> factory)
        {
            _refitMovie = RestService.For<IRefitMovie>(factory.CreateClient());
        }

        [Fact]
        public async Task GetMovies()
        {
            //Act
            var response = await _refitMovie.GetMovies();

            //Assert
            response.Should().HaveCount(16);

        }

        [Fact]
        public async Task Play()
        {
            //Arrange
            var list = await _refitMovie.GetMovies();
            var ids = list.Select(x => x.Id).Take(8);

            var request = new ChampionshipRequest
            {
                ids = ids.ToArray()
            };

            //Act
            var response = await _refitMovie.Play(request);

            //Assert
            response.Should().NotBeNull();

        }

    }
}
