﻿using Refit;
using System.Collections.Generic;
using System.Threading.Tasks;
using Movies.ChampionShip.Application.Dtos.Request;
using Movies.ChampionShip.Application.Dtos.Response;

namespace Movies.Championship.IntegrationTest.Base.Refit
{
    public interface IRefitMovie
    {
        [Get("/api/movie")]
        Task<IEnumerable<MovieResponse>> GetMovies();

        [Post("/api/movie/play")]
        Task<ChampionshipResponse> Play(ChampionshipRequest request);
    }
}