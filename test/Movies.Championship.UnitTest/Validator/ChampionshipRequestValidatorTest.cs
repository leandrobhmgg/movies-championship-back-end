﻿using System.Linq;
using Xunit;
using FluentAssertions;
using Movies.Championship.UnitTest.Builders;
using Movies.ChampionShip.Application.Properties;
using Movies.ChampionShip.Application.Validator;

namespace Movies.Championship.UnitTest.Validator
{
    public class ChampionshipRequestValidatorTest
    {
        private readonly ChampionshipRequestValidator _validator;

        public ChampionshipRequestValidatorTest()
        {
            _validator = new ChampionshipRequestValidator();
        }

        [Fact]
        public void Return_sucess()
        {
            // Arrange
            var request = new ChampionshipRequestBuilder().Build();

            // Act
            var validate = _validator.Validate(request);

            // Assert
            validate.IsValid.Should().BeTrue();

        }

        [Fact]
        public void Return_Error_Number_Of_Movies()
        {
            // Arrange
            var request = new ChampionshipRequestBuilder().WithNumberOfMovies(7).Build();

            // Act
            var validationResult = _validator.Validate(request);
            
            // Assert
            validationResult.Errors.Select(x => x.ErrorMessage).Should()
               .BeEquivalentTo(Resources.ValidateLengthChampionshipRequestIds);
        }

    }
}
