﻿using Bogus;
using Movies.ChampionShip.Application.Dtos.Request;
using System.Linq;
using Movies.Championship.UnitTest.Utils;

namespace Movies.Championship.UnitTest.Builders
{
    public class ChampionshipRequestBuilder
    {
        private readonly ChampionshipRequest _instance;
        private readonly Faker _faker = new Faker(Constants.Locale);
        public ChampionshipRequestBuilder()
        {
            var list = Enumerable.Range(0, 8)
                        .Select(_ => _faker.Random.Guid().ToString())
                        .ToArray();
            _instance = new ChampionshipRequest
            {
                ids = list
            };
        }

        public ChampionshipRequestBuilder WithNumberOfMovies(int number = 0)
        {
            _instance.ids = Enumerable.Range(0, number)
                        .Select(_ => _faker.Random.Guid().ToString())
                        .ToArray();

            return this;
        }

        public ChampionshipRequest Build()
        {
            return _instance;
        }

    }
}
