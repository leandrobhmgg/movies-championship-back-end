﻿using Bogus;
using Movies.ChampionShip.Application.Core.Entities;
using System;
using System.Collections.Generic;
using Movies.Championship.UnitTest.Utils;

namespace Movies.Championship.UnitTest.Fakers
{
    public static class MovieFaker
    {
        public static IEnumerable<Movie> GenerateFake(int number = 1)
        {
            var MovieFaker = new Faker<Movie>(Constants.Locale)
                .CustomInstantiator(f => new Movie())
                 .Rules((y, x) =>
                 {
                     x.Id = y.Random.Guid().ToString();
                     x.Title = y.Name.FullName();
                     x.Score = y.Random.Decimal(1, 10);
                     x.Year = (short)DateTime.Now.Year;

                 });

            return MovieFaker.Generate(number);
        }
    }
}
