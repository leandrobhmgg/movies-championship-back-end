﻿using System.Linq;
using Xunit;
using FluentAssertions;
using Movies.Championship.UnitTest.Fakers;
using Movies.ChampionShip.Application.Core.Entities;

namespace Movies.Championship.UnitTest.Core
{
    public class MovieMatchTest
    {

        [Fact]
        public void Return_Winner()
        {
            //Arrange 
            var first = MovieFaker.GenerateFake().First();
            var last = MovieFaker.GenerateFake().Last();

            //Act
            var movieMatch = new MovieMatch(first, last);

            //Assert
            var winner = first.Score > last.Score ? first : last;
            movieMatch.Winner.Should().Be(winner);
        }
    }
}