﻿using System.Linq;
using Xunit;
using FluentAssertions;
using Movies.Championship.UnitTest.Fakers;
using Movies.ChampionShip.Application.Core.Entities;
using Movies.ChampionShip.Application.Properties;

namespace Movies.Championship.UnitTest.Core
{
    public class MovieChampionshipTest
    {
        private readonly MovieChampionship _movieChampionship;
        public MovieChampionshipTest()
        {
            _movieChampionship = new MovieChampionship();
        }

        [Fact]
        public void Play_Return_Champion_Sucess()
        {
            //Arrange
            var movies = MovieFaker.GenerateFake(8);

            var expected = movies.First();
            expected.Score = 11;

            //Act
            var firstMovie = _movieChampionship.Play(movies);

            //Assert
            firstMovie.Winner.Id.Should().Be(expected.Id);
        }

        [Fact]
        public void Play_Execption_Number_Movies()
        {
            //Arrange
            var movies = MovieFaker.GenerateFake(7);

            //Assert
            Assert.Throws<System.Exception>(() =>
            {
                //Act
                _movieChampionship.Play(movies);

            }).Message.Should().Be(Resources.NumberMoviesMustBeEven);

        }
    }
}
